import { BASE_URL } from '../config';
import axios from 'axios';
import { store } from '../store/store';

export async function  generateToken(id) {
    let token = await axios.post(`${BASE_URL}/get-token/${id}`);
    console.log('token', token);
    store.dispatch({
        type : 'AUTH_TOKEN',
        payload: token.data
    })
}