import React from 'react';
import { connect } from 'react-redux'
import { generateToken } from './actions/index';
import ChatBody from './ChatScreen/ChatBody'
import ChatHeader from './ChatScreen/ChatHeader';
import ChatFooter from './ChatScreen/ChatFooter';

let chatClientValue = false;
let chatClient;
let chatChannel;

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      textInput: '',
      chatMessages: [],
      searchMessage: '',
      openSearch: false
    }
    this.messagesEnd = React.createRef()
  }
  componentDidMount() {
    let identity = '123'
    generateToken(identity);
  }

  componentDidUpdate() {
    const { token } = this.props;
    if (token && token.jwt && !chatClientValue) {

      chatClientValue = true
      if (chatClient) {
        return null
      }
      else {
        window.Twilio.Chat.Client.create(token.jwt).then(client => {
          console.log(client);
          chatClient = client;

          chatClient.on('channelAdded', (channel) => {
            console.log('Channel added: ' + channel.friendlyName);
          });


          chatClient.on('channelJoined', (channel) => {
            chatChannel = channel;
            console.log('Joined channel ' + channel.friendlyName);
            this.getMessages()
          });

          chatClient.on('messageAdded', (message) => {
            this.setState({
              chatMessages: [...this.state.chatMessages, message.state]
            })
          });

          this.getChannel();
        });
      }
    }
    this.scrollToBottom();
  }

  getChannel = () => {
    chatClient.getUserChannelDescriptors().then((paginator) => {
      for (let i = 0; i < paginator.items.length; i++) {
        var channel = paginator.items[i];
        console.log('Channel: ', paginator.items[i]);
        if (channel.status !== 'joined') {
          chatChannel = channel;
          channel.join();
        }
      }
    });
  }

  createChannel = () => {
    chatClient
      .createChannel({
        uniqueName: 'general',
        friendlyName: 'General Chat Channel',
      })
      .then((channel) => {
        console.log('Created general channel:');
        channel.join();
        console.log(channel);
      });
  }

  sendMessage = () => {
    chatChannel.sendMessage(this.state.textInput);
    this.setState({ textInput: '' })
  }
  chatMessage = (event) => {
    this.setState({
      textInput: event.target.value
    })
  }

  getMessages = () => {
    chatChannel.getMessages().then((messages) => {
      const totalMessages = messages.items.length;
      for (let i = 0; i < totalMessages; i++) {
        const message = messages.items[i];
        console.log('Author:' + message.author);
        this.setState({
          chatMessages: [...this.state.chatMessages, message.state]
        })
      }
     
      console.log('Total Messages:' + totalMessages);
    });
  }

  searchingMessage = (event) => {
    this.setState({
      searchMessage: event.target.value
    })
  }
  openSearch = () => {
    const { openSearch } = this.state;
    if(openSearch) {
    this.setState({
      openSearch: false,
      searchMessage: ''
    })
  }
  else {
    this.setState({
      openSearch: true
    })
  }
}

scrollToBottom = () => {
  console.log(this.messagesEnd, 'sdsdss');
  if(this.messagesEnd && this.messagesEnd.current)
  this.messagesEnd.current.scrollIntoView({ behavior: "smooth" });
}

  render() {
    console.log('this', this.state.chatMessages);
    return (
      <div className="chat-section">
        <ChatHeader openSearch={this.openSearch}/>
        {this.state.openSearch ? <input className="search-input" type="text" placeholder="Search Message" value={this.state.searchMessage} onChange={this.searchingMessage} /> : null}
        <ChatBody 
        chatMessages={this.state.chatMessages}
        searchMessage={this.state.searchMessage}
        openSearch={this.state.openSearch}
        messagesEnd={this.messagesEnd}  
        />
        <ChatFooter
          chatMessage={this.chatMessage}
          sendMessage={this.sendMessage}
          textInput={this.state.textInput} />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.auth.token
  }
}


export default connect(mapStateToProps, {})(App);



