import React from 'react';

class ChatFooter extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="chat-footer">
                <input type="text" value={this.props.textInput} placeholder="Type a Message.." onChange={this.props.chatMessage}/>
                <span className="send-icon" onClick={this.props.sendMessage}>
                    <i className="fa fa-caret-right"></i>
                </span>
            </div>
        )
    }
}

export default ChatFooter;