import React from 'react';

const sendingMessage = ['hello', 'Whats up', 'nothing much','hello', 'Whats up', 'nothing much', 'hello man', 'all gud', 'nothing '];

class ChatBody extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { chatMessages, searchMessage, openSearch, messagesEnd } = this.props;
        console.log('se', searchMessage);
        return (
            <div  ref={messagesEnd} className="chat-body" style={openSearch ? {padding: '0px'} : null}>
                {chatMessages.length > 0 ? chatMessages.map((val, index)=>(
                    <div key={index} style={index%2 == 0 ? {textAlign: 'end'} : {textAlign: 'start'}}>
                        <div style={searchMessage !== '' && val.body.includes(searchMessage) ? { color: 'red'} : null} className={index%2 == 0 ?"sending-messages" : "receiving-message"}>
                            <span>{val.body}</span>
                        </div>
                    </div>
                )) : <div style={{ textAlign: 'center'}}>No Message </div>}
            </div> 
        )
    }
}

export default ChatBody;