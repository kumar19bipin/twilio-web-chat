import React from 'react';

class ChatHeader extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="header-chat">
                <span className="image-container"><img src="https://cdn3.iconfinder.com/data/icons/business-avatar-1/512/8_avatar-512.png" alt="avatar" /></span>
                <span>Header</span>
                <span className="header-icon">
                    <i onClick={this.props.openSearch} className="fa fa-search camera-icon"></i>
                    <i className="fa fa-paperclip"></i></span>
            </div>
        )
    }
}

export default ChatHeader;