const defaultValues = {
    token: ''
}

export default function counter(state = defaultValues, action) {
    switch (action.type) {
      case 'AUTH_TOKEN':
        return { state, token: action.payload }
      default:
        return state
    }
  }